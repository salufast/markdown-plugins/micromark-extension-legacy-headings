/**
 * @typedef {import('micromark-util-types').Extension} Extension
 * @typedef {import('micromark-util-types').Token} Token
 * @typedef {import('micromark-util-types').Tokenizer} Tokenizer
 * @typedef {import('micromark-util-types').Resolver} Resolver
 * @typedef {import('micromark-util-types').State} State
 */

import {factorySpace} from 'micromark-factory-space'
import {markdownLineEnding, markdownSpace} from 'micromark-util-character'
import {splice} from 'micromark-util-chunked'
import {codes} from 'micromark-util-symbol/codes.js'
import {constants} from 'micromark-util-symbol/constants.js'
import {types} from 'micromark-util-symbol/types.js'

/**
 * @returns {Extension}
 */
export function legacyHeadings() {
  const legacyHeadings = {
    name: 'legacyHeadings',
    tokenize: tokenizeHeadings,
    resolve: resolveHeadings
  }
  return {
    flow: {[codes.numberSign]: legacyHeadings}
  }
}

/** @type {Resolver} */
function resolveHeadings(events, context) {
  const contentEnd = events.length - 4
  let contentStart = 3
  /** @type {Token} */
  let content
  /** @type {Token} */
  let text

  // Prefix whitespace, part of the opening.
  if (events[contentStart][1].type === types.whitespace) {
    contentStart += 2
  }

  if (contentEnd > contentStart) {
    content = {
      type: types.atxHeadingText,
      start: events[contentStart][1].start,
      end: events[contentEnd][1].end
    }
    text = {
      type: types.chunkText,
      start: events[contentStart][1].start,
      end: events[contentEnd][1].end,
      // @ts-expect-error Constants are fine to assign.
      contentType: constants.contentTypeText
    }

    splice(events, contentStart, contentEnd - contentStart + 1, [
      ['enter', content, context],
      ['enter', text, context],
      ['exit', text, context],
      ['exit', content, context]
    ])
  }

  return events
}

/** @type {Tokenizer} */
function tokenizeHeadings(effects, ok, nok) {
  const maxLevel = 2
  let level = 0
  let hasText = false

  return start

  /** @type {State} */
  function start(code) {
    effects.enter(types.atxHeading)
    effects.enter(types.atxHeadingSequence)
    return fenceOpen(code)
  }

  /** @type {State} */
  function fenceOpen(code) {
    if (code === codes.numberSign && level++ < maxLevel) {
      effects.consume(code)
      return fenceOpen
    }

    if (
      code !== codes.numberSign &&
      code !== codes.eof &&
      !markdownLineEnding(code)
    ) {
      effects.exit(types.atxHeadingSequence)
      return headingBreak(code)
    }

    return nok(code)
  }

  /** @type {State} */
  function headingBreak(code) {
    if (markdownSpace(code)) {
      return factorySpace(effects, headingBreak, types.whitespace)(code)
    }

    effects.enter(types.atxHeadingText)
    return data(code)
  }

  /** @type {State} */
  function data(code) {
    if (code === codes.numberSign && hasText) {
      effects.exit(types.atxHeadingText)
      effects.enter(types.atxHeadingSequence)
      return fenceClose(code)
    }

    if (
      code !== codes.numberSign &&
      code !== codes.eof &&
      !markdownLineEnding(code)
    ) {
      effects.consume(code)
      hasText = true
      return data
    }

    return nok(code)
  }

  /** @type {State} */
  function fenceClose(code) {
    if (code === codes.numberSign && level-- >= 0) {
      effects.consume(code)
      return fenceClose
    }

    if (level === 0) {
      effects.exit(types.atxHeadingSequence)
      effects.exit(types.atxHeading)
      return ok(code)
    }

    return nok(code)
  }
}
