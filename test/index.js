import fs from 'fs'
import path from 'path'
import test from 'tape'
import {micromark} from 'micromark'
import {legacyHeadings as syntax} from '../dev/index.js'

test('markdown -> html (micromark)', (t) => {
  /**
   * @param s {string}
   */
  const m = (s) =>
    micromark(s, {
      extensions: [syntax()]
    })

  t.deepEqual(
    m('#heading#'),
    '<h1>heading</h1>',
    'should parse legacy h1 headings'
  )
  t.deepEqual(
    m('# heading#'),
    '<h1>heading</h1>',
    'should parse legacy h1 headings with prefix whitespace'
  )
  t.deepEqual(
    m('#heading #'),
    '<h1>heading</h1>',
    'should parse legacy h1 headings with suffix whitespace'
  )
  t.deepEqual(
    m('# heading #'),
    '<h1>heading</h1>',
    'should parse legacy h1 headings with prefix and suffix whitespace'
  )
  t.deepEqual(
    m('##heading##'),
    '<h2>heading</h2>',
    'should parse legacy h2 headings'
  )
  t.deepEqual(
    m('## heading##'),
    '<h2>heading</h2>',
    'should parse legacy h2 headings with prefix whitespace'
  )
  t.deepEqual(
    m('##heading ##'),
    '<h2>heading</h2>',
    'should parse legacy h2 headings with suffix whitespace'
  )
  t.deepEqual(
    m('## heading ##'),
    '<h2>heading</h2>',
    'should parse legacy h2 headings with prefix and suffix whitespace'
  )
  t.deepEqual(
    m("#!\"§$%&/()=?{}[]\\'`°^+*~'’,;.:-_<>|#"),
    "<h1>!&quot;§$%&amp;/()=?{}[]'`°^+*~'’,;.:-_&lt;&gt;|</h1>",
    'should parse headings containing punctuation'
  )
  t.deepEqual(
    m('#äöüßÄÖÜẞ#'),
    '<h1>äöüßÄÖÜẞ</h1>',
    'should parse german mutated vowels'
  )
  t.deepEqual(
    m('#heading\n'),
    '<p>#heading</p>\n',
    'should not allow newlines in heading'
  )

  t.deepEqual(
    m('###heading###'),
    '<p>###heading###</p>',
    'should not parse h3 headings'
  )

  t.deepEqual(
    m('##heading#'),
    '<p>##heading#</p>',
    'should not allow less closing than opening fences'
  )
  t.deepEqual(
    m('#heading##'),
    '<p>#heading##</p>',
    'should not allow more closing than opening fences'
  )
  t.deepEqual(m('# #'), '<h1></h1>', 'should parse empty heading')
  t.deepEqual(
    m('#h e a d i n g    #'),
    '<h1>h e a d i n g</h1>',
    'should cut off suffix whitespaces'
  )

  t.deepEqual(
    m('# heading 1'),
    '<h1>heading 1</h1>',
    'should not interfere with standard md headings (1)'
  )

  t.deepEqual(
    m('## heading 2'),
    '<h2>heading 2</h2>',
    'should not interfere with standard md headings (2)'
  )

  t.deepEqual(
    m('### heading 3'),
    '<h3>heading 3</h3>',
    'should not interfere with standard md headings (3)'
  )

  t.deepEqual(
    m('#### heading 4'),
    '<h4>heading 4</h4>',
    'should not interfere with standard md headings (4)'
  )

  t.deepEqual(
    m('##### heading 5'),
    '<h5>heading 5</h5>',
    'should not interfere with standard md headings (5)'
  )

  t.deepEqual(
    m('###### heading 6'),
    '<h6>heading 6</h6>',
    'should not interfere with standard md headings (6)'
  )

  t.end()
})

test('fixtures', (t) => {
  const base = path.join('test', 'fixtures')
  const files = fs.readdirSync(base).filter((d) => /\.md$/.test(d))
  let index = -1

  while (++index < files.length) {
    const name = path.basename(files[index], '.md')
    const input = fs.readFileSync(path.join(base, name + '.md'))
    const actual = micromark(input, {
      extensions: [syntax()]
    })
    /** @type {string|undefined} */
    let expected

    try {
      expected = String(fs.readFileSync(path.join(base, name + '.html')))
    } catch {}

    if (expected) {
      t.deepEqual(actual, expected, name)
    } else {
      fs.writeFileSync(path.join(base, name + '.html'), actual)
    }
  }

  t.end()
})
